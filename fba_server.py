from __future__ import print_function

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import sys
import pickledb
import os

PORT_NUMBER_AS_CMD_ARG = 'Please enter the port number  as Command Line Argument'

global port
global host
host="224.3.29.71"
global db
global voteCount
voteCount = {}
global votingAccept
votingAccept = {}
global messageStored
messageStored = {}
global primaryServer
primaryServer = {}
global confirmationCount
confirmationCount = {}
global confirmationSent
confirmationSent = {}

class MulticastServer(DatagramProtocol):

    def startProtocol(self):
        """
        Called after protocol has started listening.
        """
        # Set the TTL>1 so multicast will cross router hops:
        self.transport.setTTL ( 5 )
        # Join a specific multicast group:
        self.transport.joinGroup ( "224.3.29.71" )
    global data

    temp=0
    def sendMessage(self, msg):
        self.transport.write ( msg.encode ( "utf-8" ), (("224.3.29.71"), 3000) )
        self.transport.write ( msg.encode ( "utf-8" ), (("224.3.29.71"), 3001) )
        self.transport.write ( msg.encode ( "utf-8" ), (("224.3.29.71"), 3002) )
        self.transport.write ( msg.encode ( "utf-8" ), (("224.3.29.71"), 3003) )

    def printDB(self):
        keys = db.getall()
        print('********* DB status **********')
        for key in keys:
            print('User: ', key, ', Balance: ', db.get(key))
    def datagramReceived(self, datagram, address):
        print ( 'Datagram  %s received from %s' % (repr ( datagram ), repr ( address )) )
        stringMessage=datagram.decode("utf-8")
        if '@' not in stringMessage:
            primaryServer[address] = 'true'
            msg = 'pleaseVote@' + stringMessage;
            self.sendMessage(msg)
            return
        else:
            if 'pleaseVote@' in stringMessage:
                msg = stringMessage.replace('pleaseVote@','voting@')
                self.sendMessage(msg)
                return
            elif 'voting@' in stringMessage:
                if stringMessage in voteCount:
                    count = int(voteCount[stringMessage])
                    count = count + 1;
                    voteCount[stringMessage] = count
                else:
                    voteCount[stringMessage] = 1
                    votingAccept[stringMessage] = 'false'
                if votingAccept[stringMessage] == 'false' and voteCount[stringMessage] >= 3:
                    msg = stringMessage.replace ( 'voting@', 'pleaseAccept@' )
                    self.sendMessage(msg)
                    votingAccept[stringMessage] = 'true'
                return
            elif 'pleaseAccept@' in stringMessage:
                msg = stringMessage.replace('pleaseAccept@', '')
                if msg in messageStored and messageStored[msg] == 'true':
                    return
                data = msg.split ( ':' )
                dataKey = data[0]
                dataValue = data[1]
                keys = db.getall ()
                if (dataKey in keys):
                    dbValue = db.get ( dataKey )
                    dbValue = dbValue[1:]
                    intDBValue = int ( dbValue )
                    dataValue = dataValue[1:]
                    intDataValue = int ( dataValue )
                    intDBValue += intDataValue
                    dbValue = str ( intDBValue )
                    dbValue = "$" + dbValue
                    db.set ( dataKey, dbValue )
                else:
                    db.set(dataKey, dataValue)
                db.dump ()
                self.printDB()
                messageStored[msg] = 'true'
                msg = stringMessage.replace ( 'pleaseAccept@', 'confirming@' )
                self.sendMessage(msg)
                return
            elif 'confirming@' in stringMessage:
                if stringMessage in confirmationCount:
                    count = int(confirmationCount[stringMessage])
                    count = count + 1;
                    confirmationCount[stringMessage] = count
                else:
                    confirmationCount[stringMessage] = 1
                    confirmationSent[stringMessage] = 'false'
                if confirmationSent[stringMessage] == 'false' and confirmationCount[stringMessage] >= 3:
                    msg = stringMessage.replace ( 'confirming@', 'confirmingToClient@' )
                    if ('192.168.0.17', int(port)) in primaryServer and primaryServer[('192.168.0.17', int(port))] == 'true':
                        self.sendMessage(msg)
                    confirmationSent[stringMessage] = 'true'
                return

if __name__ == '__main__':
    cmdArgs = sys.argv
    if (len ( cmdArgs ) < 2):
        print ( PORT_NUMBER_AS_CMD_ARG )
        exit ( 1 )
    port = cmdArgs[1]
    host = "224.3.29.71"
    fileName = 'assignment3_' + str ( port ) + '.db'
    if os.path.exists ( fileName ):
        os.remove ( fileName )
        db = pickledb.load ( fileName, False )
    else:
        db = pickledb.load ( fileName, False )
    reactor.listenMulticast ( int ( port ), MulticastServer (), listenMultiple=True )
    reactor.run ()

