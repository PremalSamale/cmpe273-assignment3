from __future__ import print_function

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import sys


PORT_NUMBER_AS_CMD_ARG = 'Please enter the port number  as Command Line Argument'
global host

host = "224.3.29.71"
global data
data = []
global messageCount
messageCount = {}

class MulticastClient(DatagramProtocol):
    def startProtocol(self):
        self.transport.joinGroup ( "224.3.29.71" )
        data.append('foo:$10')
        data.append('bar:$30')
        data.append('foo:$20')
        data.append('bar:$20')
        data.append('foo:$30')
        data.append('bar:$10')
        messageCount['count'] = 0
        self.transport.write ( data[messageCount['count']].encode ( "utf-8" ), (("224.3.29.71"), int ( port )) )
        messageCount['count'] = messageCount['count'] + 1

    def datagramReceived(self, datagram, address):
        stringMessage = datagram.decode ( "utf-8" )
        stringAddress = str(address)
        if 'confirmingToClient@' in stringMessage and str(port) in stringAddress and messageCount['count'] <= 6:
            if messageCount['count'] <= 5:
                print ( 'Datagram  %s received from %s' % (repr ( datagram ), repr ( address )) )
                self.transport.write ( data[messageCount['count']].encode ( "utf-8" ), (("224.3.29.71"), int ( port )) )
                messageCount['count'] = messageCount['count'] + 1
            else:
                if messageCount['count'] ==6:
                    print ( 'Datagram  %s received from %s' % (repr ( datagram ), repr ( address )) )

    # Possibly invoked if there is no server listening on the
    # address to which we are sending.
    def connectionRefused(self):
        print("No one listening")

if __name__ == '__main__':
    cmdArgs = sys.argv
    if (len ( cmdArgs ) < 2):
        print ( PORT_NUMBER_AS_CMD_ARG )
        exit ( 1 )
    global port
    port = cmdArgs[1]
    reactor.listenMulticast(int(port),MulticastClient(), listenMultiple=True)
    reactor.run()