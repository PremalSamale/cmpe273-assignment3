#cmpe273-Assignment3
#Premal Dattatray Samale
#SJSU ID:012566333



## Implemented a Federated Byzantine Agreement (FBA) prototype in Python3 using Twisted UDP Multicast.
FBA prototype implementation is on server side in the file fba_server.py.
Client side code is implemented in fba_client.py
Outputs files are showing output of client and each server on port 3000,3001,3002,3003.(3000 as primary server)
####Python version used:3.5.2
####Database Used:PickleDB
## Instruction to run the code

```
# Run the server nodes.
python3 fba_server.py 3000
python3 fba_server.py 3001
python3 fba_server.py 3002
python3 fba_server.py 3003

# Run the client.
python3 fba_client.py 3000

```
####Below is the FBA messages flow:

```
 1. Client send message one at a time to primary server for example "foo:$10" .
 
 2. Primary server receive message from client and sends message "pleaseVote@foo:$10' to every other server including himself.
 
 3. Every server start giving votes to others by sending message 'voting@foo:$10' 
 
 4. Each server check it's vote count received. If it is grater than or equal to 3(including server who is counting) then server sends 'pleaseAccept@foo:$10' to other servers. If any 1 server goes down ,it will not impact the other server.
 
 5. Server who receives 'pleaseAccept@foo:$10' message ,updates their database values. If user is already exist in DB then new balance is getting added to old balance and total value is updated in the databse. If user is new then new entry is added to DB.
    For each server  assignment3_{port_num}.db files are generated.
    
 6. After DB updates each server sends to other servers a confirmation message 'confirming@foo:$10'.
 
 7. Primary server checks the count of confirmation for each data. If count is more than equal to 3 including primary server then primary server sends reply message 'confirmingToClient@foo:$10' to client.
 
 8. Client receives reply message from primary server and sends next data and again start above process .
 
 9. At the end .db files of all four server contains same data which is {"foo": "$60", "bar": "$60"} (Attached output text files and .db files)

```



